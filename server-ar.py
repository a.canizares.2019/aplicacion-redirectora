#!/usr/bin/python

import socket
import random

#Adrian Cañizares Salgado

urls= ["https://google.com/", "https://facebook.com/","https://youtube.com/","https://www.aulavirtual.urjc.es","https://www.urjc.es/", "https://www.instagram.com/", "https://www.tiktok.com/es/"]
myPort = 1333


def red_new_url(request):
	#escojo una url aleatoria del diccionario
	new_url = random.choice(urls)
	print(new_url)
	#mesnsaje de respuesta 301 para redireccionar.
	response = "HTTP/1.1 302 Moved Temporarily \r\n" \
                   + "Location: " + str(new_url) + "\r\n" \
                   + "\r\n"
	return response.encode('utf-8')
def main():
	mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	mySocket.bind(('', myPort))
	mySocket.listen(5)
	random.seed()
	try:
		while True:
			print("Waiting for connections")
			(recvSocket, address) = mySocket.accept()
			print("HTTP request received:")
			request = recvSocket.recv(2048)
			response = red_new_url(request)
			recvSocket.send(response)
			recvSocket.close()
	except KeyboardInterrupt:
		print("Keyboard interrupt")
		mySocket.close()
if __name__== "__main__":
	main()